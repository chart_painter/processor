#!/usr/bin/env bash

args=("$@")

case "${1}" in
    "bash")
        shift
        exec bash -c "${args[@]:1}"
        ;;
    "run")
        ./wait-for-it.sh pg:5432 --timeout=30 --strict -- echo "Postgres is up"
        ./wait-for-it.sh rabbit:5672 --timeout=30 --strict -- echo "Rabbit is up"
        exec python -m application.server
        ;;
esac
