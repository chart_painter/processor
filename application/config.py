from pydantic import BaseSettings

__all__ = ["config"]


class Config(BaseSettings):
    log_level: str = "ERROR"

    pg_dsn: str
    rabbit_dsn: str
    exchange: str = "charts"
    routing_key: str = "charts"
    queue_name: str = "charts"

    collect_timeout: int = 5


config = Config()
