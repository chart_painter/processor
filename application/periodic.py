import json
import logging
from typing import List

import aio_pika
import aiojobs
import aiopg
from aio_pika import RobustConnection, RobustChannel
from psycopg2._json import Json
from psycopg2.extras import RealDictCursor

from application.config import config

logger = logging.getLogger(__name__)


class Periodic:
    @classmethod
    async def run(cls):
        scheduler = await aiojobs.create_scheduler()

        amqp_connection: RobustConnection = await aio_pika.connect_robust(config.rabbit_dsn)

        async with aiopg.create_pool(config.pg_dsn) as pg_pool:
            async with amqp_connection:
                channel: RobustChannel = await amqp_connection.channel()
                exchange = await channel.declare_exchange(config.exchange, auto_delete=False)
                queue = await channel.declare_queue(config.routing_key, auto_delete=False, durable=True)
                await queue.bind(exchange, config.queue_name)

                async with queue.iterator() as queue_iter:
                    async for message_in in queue_iter:
                        async with message_in.process():
                            message_body = message_in.body.decode()
                            try:
                                data_in = json.loads(message_body)
                            except ValueError:
                                logger.exception(f'Error at processing message "{message_body}"')
                                continue

                            await scheduler.spawn(cls._run(pg_pool, data_in))

    @classmethod
    async def _run(cls, pg_pool, params: dict) -> None:
        try:
            chart_data = await cls._calculate_chart_data(params)
            status = "success"

            logger.info(f"ChartID={params['id']} was processed succeed")
        except Exception as e:
            chart_data = []
            status = "failed"

            logger.exception(f"ChartID={params['id']} was processed failure")

        try:
            async with pg_pool.acquire() as connection:
                async with connection.cursor(cursor_factory=RealDictCursor) as cursor:
                    query = "UPDATE chart_chart SET status=%s, data=%s WHERE id=%s"
                    await cursor.execute(query, [status, Json(chart_data), params["id"]])
        except Exception as e:
            logger.exception(f"Error at update status of ChartID={params['id']}")

    @staticmethod
    async def _calculate_chart_data(params: dict) -> List[int]:
        results = []
        for x in range(0, params["max_x"], params["step"]):
            results.append(params["a"] * x ^ 3 + params["b"] * x ^ 2 + params["c"] * x + params["d"])

        return results
