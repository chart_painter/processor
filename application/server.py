import asyncio
import logging
from logging.config import dictConfig

from application.config import config
from application.periodic import Periodic

LOGGER_CONFIG = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "default": {
            "format": "[%(asctime)s] %(levelname)s [%(name)s:%(lineno)s] %(funcName)s: %(message)s",
            "datefmt": "%d.%m.%Y %H:%M:%S",
        }
    },
    "handlers": {"stdout": {"level": "INFO", "class": "logging.StreamHandler", "formatter": "default"}},
    "loggers": {"": {"handlers": ["stdout"], "propagate": True, "level": config.log_level}},
}
dictConfig(LOGGER_CONFIG)
logger = logging.getLogger(__name__)

if __name__ == "__main__":
    try:
        asyncio.run(Periodic.run())
    except KeyboardInterrupt:
        pass
    except Exception as e:
        logger.exception("Error at started server")

        raise e
