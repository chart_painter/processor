FROM python:3.7.4

ENV PYTHONDONTWRITEBYTECODE 1
ENV WORK_DIR "/application/src"

RUN pip install poetry &&\
    poetry config virtualenvs.create false

RUN mkdir -p ${WORK_DIR}
WORKDIR ${WORK_DIR}
ADD . ${WORK_DIR}

RUN poetry install --no-dev

ENTRYPOINT ["/application/src/docker-entrypoint.sh"]
CMD ["run"]
