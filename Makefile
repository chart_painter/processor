.PHONY: isort
isort:
	@echo -n "Run isort"
	isort -rc application


.black: $(shell find application -type d)
	black -l 120 application tests
	@if ! isort -c -rc application; then \
            echo "Import sort errors, run 'make isort' to fix them!!!"; \
            isort --diff -rc application; \
            false; \
	fi

.PHONY: black
black: .black
	@echo -n "Run black and isort "
